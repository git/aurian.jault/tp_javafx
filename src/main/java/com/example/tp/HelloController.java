package com.example.tp;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.UUID;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import javafx.util.converter.NumberStringConverter;

public class HelloController implements Initializable{

    @FXML
    private TreeView<iCaptor> myListTreeView;

    @FXML
    private Label myLabel;

    @FXML
    private Spinner<Integer> mySpinner;

    @FXML
    private Label myLabel2;

    @FXML
    private Rectangle myRectangle;

    int currentValue;


    String currentFood;

    @Override@FXML
    public void initialize(URL arg0, ResourceBundle arg1) {

        iCaptor it1 = new Captor("Capteur 1");
        iCaptor it2 = new Captor("Capteur 2");


        iCaptor it3 = new Captor("Capteur 3");
        iCaptor it4 = new Captor("Capteur 4");
        iCaptor it5 = new Captor("Capteur 5");

        ArrayList<iCaptor> array =  new ArrayList<iCaptor>();
        array.add(it1);
        array.add(it2);

        iCaptor capt  = new CaptorVirtual("Capteur virtuel",array);



        TreeItem<iCaptor> root = new TreeItem<iCaptor>(new Captor("Capteur 6"));
        root.getChildren().add(it3.display());
        root.getChildren().add(it4.display());
        root.getChildren().add(it5.display());
        root.getChildren().add(capt.display());

        myListTreeView.setRoot(root);

        myListTreeView.setShowRoot(false);


        SpinnerValueFactory<Integer> valueFactory =
                new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 30);

        valueFactory.setValue(1);

        mySpinner.setValueFactory(valueFactory);

        currentValue = mySpinner.getValue();

        myLabel2.setText(Integer.toString(currentValue));

        mySpinner.valueProperty().addListener(new ChangeListener<Integer>() {

            @Override
            public void changed(ObservableValue<? extends Integer> arg0, Integer arg1, Integer arg2) {

                currentValue = mySpinner.getValue();

                update(mySpinner.getValue());

                myLabel2.setText(Integer.toString(currentValue));

            }
        });

    }
    public void update(int value){
        if (value < 5)
        {
            myRectangle.setFill(Color.BLUE);
        }
        else if (value < 15)
        {
            myRectangle.setFill(Color.ORANGE);
        }
        else
        {
            myRectangle.setFill(Color.RED);
        }
    }

    public void selectItem() {
        myListTreeView.getSelectionModel().selectedItemProperty().addListener((observableValue, captorTreeItem, t1) -> {
            StringConverter<Number> coucou = new NumberStringConverter();
            if (captorTreeItem == null)return;
            myLabel.textProperty().bind(captorTreeItem.getValue().temp.asString());

        });
    }
}