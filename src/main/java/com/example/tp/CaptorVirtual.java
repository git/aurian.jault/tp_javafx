package com.example.tp;

import javafx.scene.control.TreeItem;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class CaptorVirtual extends iCaptor {

    ArrayList<iCaptor> array = new ArrayList<iCaptor>();

    public CaptorVirtual(String nom,ArrayList<iCaptor> tab)
    {
        super(nom);
        array = tab;
    }

    @Override
    public TreeItem<iCaptor> display() {
        TreeItem<iCaptor> root = new TreeItem<>(this);
        for (iCaptor cpt: array)
        {
            root.getChildren().add(cpt.display());
        }
        return root;
    }

    public void add(iCaptor capt)
    {
        array.add(capt);
    }
}
