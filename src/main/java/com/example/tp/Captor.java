package com.example.tp;

import javafx.application.Platform;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.TreeItem;

import java.util.Random;

public class Captor extends iCaptor {
    Captor(String nom)
    {
        super(nom);
    }

    @Override
    public TreeItem<iCaptor> display() {
        return new TreeItem<iCaptor>(this);
    }
}
