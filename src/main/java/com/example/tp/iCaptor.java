package com.example.tp;

import javafx.application.Platform;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.TreeItem;

import java.util.Random;

public abstract class iCaptor {
    SimpleStringProperty nom = new SimpleStringProperty();

    SimpleDoubleProperty temp = new SimpleDoubleProperty();


    public void genTemp(){};

    public iCaptor(String nom)
    {
        this.nom.setValue(nom);
        this.temp.setValue(new Random().nextDouble() * 30);
        Thread t1 = new Thread(
                ()->{
                    while(true) {
                        try {
                            Thread.sleep(1000);
                            Platform.runLater(()->{
                                this.temp.setValue(new Random().nextDouble() * 30);
                            });
                        } catch (InterruptedException e) {
                            throw new RuntimeException(e);
                        }
                    }
                }
        );
        t1.start();
    }

    public abstract TreeItem<iCaptor> display();
    @Override
    public String toString()
    {
        return this.nom.getValue();
    }
}
